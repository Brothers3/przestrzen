package swiat;

public class Punkt {

	public long x;
	public long y;
	public long z;

	public Punkt(long x, long y, long z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String toString() {
		String output = "(" + x + "," + y + "," + z + ")";
		return output;
	}

}
