package swiat;

public class Prosta {
	private Punkt punkt1;
	private Punkt punkt2;
	long tx;
	long ty;
	long tz;
	long A;
	long B;
	long C;

	public Prosta(Punkt punkt1, Punkt punkt2) {

		this.punkt1 = punkt1;
		this.punkt2 = punkt2;
		rownanie();
	}

	public String rownanie() {
		String output;
		if (punkt1.x == punkt2.x && punkt1.y == punkt2.y && (punkt1.z == punkt2.z)) {
			output = " LUL";
		}

		else if (punkt1.x == punkt2.x) {
			B = -punkt1.y;
			ty = (punkt2.y - punkt1.y);
			C = -punkt1.z;
			tz = (punkt2.z - punkt1.z);
			output = ty + "t= y " + B + "\n" + tz + "t= z" + C;
		} else if (punkt1.y == punkt2.y) {

			A = -punkt1.x;
			tx = (punkt2.x - punkt1.x);
			C = -punkt1.z;
			tz = (punkt2.z - punkt1.z);
			output = tx + "t= x " + A + "\n" + tz + "t= z" + C;

		} else if (punkt1.z == punkt2.z) {
			A = -punkt1.x;
			tx = (punkt2.x - punkt1.x);
			B = -punkt1.y;
			ty = (punkt2.y - punkt1.y);
			output = tx + "t= x " + A + "\n" + ty + "t= y " + B;

		} else {
			A = -punkt1.x;
			tx = (punkt2.x - punkt1.x);
			B = -punkt1.y;
			ty = (punkt2.y - punkt1.y);
			C = -punkt1.z;
			tz = (punkt2.z - punkt1.z);
			output = tx + "t= x " + A + "\n" + ty + "t= y " + B + "\n" + tz + "t= z " + C;

		}

		return output;
	}

	public boolean czyNalezyDoProstejPunkt(Punkt punkt) {
		if (tx != 0 && ty != 0 && tz != 0) {
			long t1 = (punkt.x + A) / tx;
			long t2 = (punkt.y + B) / ty;
			long t3 = (punkt.z + C) / tz;
			if (t1 == t2 && t1 == t3) {
				return true;
			} else {
				return false;
			}
		} else if (tx != 0 && ty != 0) {
			long t1 = (punkt.x + A) / tx;
			long t2 = (punkt.y + B) / ty;
			if (t1 == t2) {
				return true;
			} else {
				return false;
			}

		} else if (tx != 0 && tz != 0) {
			long t1 = (punkt.x + A) / tx;
			long t3 = (punkt.z + C) / tz;
			if (t1 == t3) {
				return true;
			} else {
				return false;
			}
		} else if (ty != 0 && tz != 0) {
			long t2 = (punkt.y + B) / ty;
			long t3 = (punkt.z + C) / tz;
			if (t2 == t3) {
				return true;
			} else {
				return false;
			}

		}

		return false;
	}

	public double odlegloscPunktuOdProstej(Punkt punkt) {
		if (czyNalezyDoProstejPunkt(punkt)) {
			return 0;
		} else {
			long wX = punkt2.x - punkt.x;
			long wY = punkt2.y - punkt.y;
			long wZ = punkt2.z - punkt.z;

			double distanceUP = Math.sqrt(
					((wY * tz - (wZ * ty)) * (wY * tz - (wZ * ty))) + ((wZ * tx - (wX * tz)) * (wZ * tx - (wX * tz)))
							+ ((wX * ty - (wY * tx)) * (wX * ty - (wY * tx))));
			System.out.println("wewnatrz : " + distanceUP);
			double distanceDown = Math.sqrt((A * A) + (B * B) + (C * C));
			System.out.println("wewnatrz2 : " + distanceDown);
			distanceUP = distanceUP / distanceDown;
			return distanceUP;
		}

	}

}
